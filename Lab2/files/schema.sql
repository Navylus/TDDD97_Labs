DROP TABLE IF EXISTS messages;
DROP TABLE IF EXISTS user;
create table user (
    email text primary key,
    password text,
    token text,
    gender text,
    city text,
    country text,
    lastName text,
    firstName text);


create table messages (
    email text,
    content text);