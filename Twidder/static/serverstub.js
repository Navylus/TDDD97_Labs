/**
 * Serverstub.js
 * MODIFIED TO USE server.py
 **/
var serverstub = (function() {


  var serverstub = {
      signIn: function (email, password) {
        var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/sign_in";
        var letters = "abcdefghiklmnopqrstuvwwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        var token = "";
        for (var i = 0 ; i < 36 ; ++i) {
          token += letters[Math.floor(Math.random() * letters.length)];
        }

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                res = JSON.parse(req.responseText);
                if(res.success === true){
                    localStorage.setItem('auth', res.data);
                    openConnection(email);
                    displayView();
                }else{
                    document.getElementById('feedBack').innerText = res.message;
                }
            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify({email:email, password:password,token:token}));
      },

      postMessage: function (token, message, email,isOther) {
        var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/post_message";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                res = JSON.parse(req.responseText);
                if(isOther)
                    showOtherWall(email);
                else
                    showWall();
            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify({token:token,message:message,email:email}));
        return res;
      },

      getUserDataByTokenAndPost: function () {
          var req = new XMLHttpRequest();
        var current = document.location.href;
        var token = localStorage.getItem("auth");
        var url = current+"/get_user_data_by_token";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                res = JSON.parse(req.responseText).data;
                var message = document.getElementById("messageText").value;
                var currentEmail = res.email;
                if(message !== ""){
                    serverstub.postMessage(token,message,currentEmail,false);
                }

                document.getElementById("messageText").value = "";
            }
        };
        req.open("GET", url+"?token="+token, true);
        req.send();
        return res;
      },

      getUserDataByEmail: function (token, email) {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/get_user_data_by_email";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                res = JSON.parse(req.responseText);
            }
        };
        req.open("GET", url+"?token="+token+"&email="+email, true);
        req.send();
        return res;
      },

      getUserMessagesByToken: function () {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/get_user_messages_by_token";
        var token = localStorage.getItem('auth');

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                var messages = JSON.parse(req.responseText);
                var wall = document.getElementById("wall");
                wall.innerHTML ="";
                if(messages.success){
                    for(var i=0;i<=messages.data.length;i++){
                        if(messages.data[i] !== undefined){
                            item = document.createElement('div');
                            item.className = "message";
                            item.appendChild(document.createTextNode("From : "));
                            item.appendChild(document.createTextNode(messages.data[i].email));
                            item.appendChild(document.createElement("br"));
                            item.appendChild(document.createTextNode(messages.data[i].content));
                            wall.appendChild(item);
                        }
                    }
                }

            }
        };
        req.open("GET", url+"?token="+token, true);
        req.send();
        return res;
      },

      getUserMessagesByEmail: function (token, email) {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/get_user_messages_by_email";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
               var messages = JSON.parse(req.responseText);
                var wall = document.getElementById("otherWall");
                wall.innerHTML ="";
                if(messages.success){
                    for(var i=0;i<=messages.data.length;i++){
                        if(messages.data[i] !== undefined){
                            item = document.createElement('div');
                            item.className = "message";
                            item.appendChild(document.createTextNode("From : "));
                            item.appendChild(document.createTextNode(messages.data[i].email));
                            item.appendChild(document.createElement("br"));
                            item.appendChild(document.createTextNode(messages.data[i].content));
                            wall.appendChild(item);
                        }
                    }
                }
            }
        };
        req.open("GET", url+"?token="+token+"&email="+email, true);
        req.send();
        return res;
      },

      signOut: function (token) {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/sign_out";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                res = JSON.parse(req.responseText);
                localStorage.setItem('auth', null);
                displayView();

            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify({token:token}));
        return res;
      },

      signUp: function (inputObject) { // {email, password, firstname, familyname, gender, city, country}
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/sign_up";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                res = JSON.parse(req.responseText);
                document.getElementById('emailSignIn').value = "";
                document.getElementById('passwordSignIn').value = "";
                document.getElementById('repeatpassword').value = "";
                document.getElementById('firstname').value = "";
                document.getElementById('lastname').value = "";
                document.getElementById('gender').value = "";
                document.getElementById('city').value = "";
                document.getElementById('country').value = "";
                document.getElementById('feedBack').innerText = res.message;
            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify(inputObject));
        return res;
      },

      changePassword: function (token, oldPassword, newPassword) {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/change_password";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                res = JSON.parse(req.responseText);
                document.getElementById('feedbackProfile').innerText = res.message;
                document.getElementById('old').value = '';
                document.getElementById('new').value = '';
                document.getElementById('newConfirm').value = '';
            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify({token:token, oldPassword:oldPassword, newPassword:newPassword}));
        return res;
      }
  };

  return serverstub;
})();