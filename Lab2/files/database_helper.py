import sqlite3
import flask
def connect_db():
    return sqlite3.connect("mydatabase.db")
def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = g.db = connect_db()
    return db

def init():
    c = get_db()
    c.execute("create table entries (id integer primary key, name text,message text)")
    c.execute("create table entries (id integer primary key, name text,message text)")
    c.commit()

def add_message(name,message):
    c = get_db()
    c.execute("insert into entries (name,message) values (?,?)", (name,message))
    c.commit()

def close():
    get_db().close()