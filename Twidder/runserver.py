from gevent import pywsgi
from server import app

server = pywsgi.WSGIServer( ('',8080), app)

server.serve_forever()
