import json
import hashlib
import sqlite3
import database_helper
from flask import g
from uuid import uuid4
from flask import Flask
from flask import jsonify
from flask import request
from gevent import pywsgi
from flask import Response
from flask import current_app
from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler


app = Flask(__name__, static_url_path='/static')




#WEB SOCKETS PART

# List to store userWebSocket objects inside
openSessions = []

@app.route('/api')
def api():
    try :
        if request.environ.get('wsgi.websocket'):
            ws = request.environ['wsgi.websocket']

            while True:

                email = ws.receive()

                if email is not None:
                    ws.send(json.dumps("Check open sessions for: " + email))

                    for uws in openSessions:
                        ws.send(json.dumps("Now in UWSList: " + uws.email))

                        if uws.email == email:
                            ws.send(json.dumps("Found open session! Ask to close old session!"))
                            uws.ws.send(json.dumps("Attempt to open new session!"))
                            uws.ws.send(json.dumps("LOGOUT"))
                            #uws.ws.close()
                            openSessions.remove(uws)

                    openSessions.append(make_session(email, ws))
                    ws.send(json.dumps("UserWebSocket object created and added to list"))
        return "OK"
    except :
        print(sys.exc_info())
        return "ERROR"


#Combines user email (email) and web socket (ws) for connection to this user's session
class Session(object):
    email = ""
    ws = ""

    # Class initializer
    def __init__(self, email, ws):
        self.email = email
        self.ws = ws

    def printOut(self):
        print("email : " + self.email)


#Creates UserWebSocket object

def make_session(email, ws):
    uws = Session(email, ws)
    uws.printOut()
    return uws

#WEBSITE PART
@app.route("/")
def home():
    return current_app.send_static_file("client.html")

@app.route("/sign_up", methods=['POST','GET'])
def sign_up():
    if request.method == 'POST':
        database_helper.connect_db()
        resJson = request.get_json()
        return jsonify(database_helper.sign_up(resJson["email"], resJson["password"], resJson["firstname"], resJson["familyname"], resJson["gender"], resJson["city"] , resJson["country"])), 200
    else :
        return "need POST Method", 404


@app.route("/sign_in", methods=['POST','GET'])
def sign_in():
    if request.method == 'POST':
        database_helper.connect_db()
        resJson = request.get_json()
        return jsonify(database_helper.sign_in(resJson["email"],resJson["password"],resJson["token"])), 200
    else:
        return "need POST Method", 400


@app.route("/addTest")
def addContact():
    database_helper.connect_db()
    insertTestUser()
    return "added", 200

@app.route("/sign_out", methods=['POST','GET'])
def sign_out():
    if request.method == 'POST':
        database_helper.connect_db()
        resJson = request.get_json()
        return jsonify(database_helper.sign_out(resJson["token"])), 200
    else:
        return "need POST Method", 404


@app.route("/change_password", methods=['POST','GET'])
def change_password():
    if request.method == 'POST':
        database_helper.connect_db()
        resJson = request.get_json()
        return jsonify(database_helper.change_password(resJson["token"],resJson["oldPassword"],resJson["newPassword"])), 200
    else:
        return "need POST Method", 404

@app.route("/get_user_data_by_token")
def get_user_data_by_token():
    database_helper.connect_db()
    token = request.args.get('token')
    return  jsonify(database_helper.get_user_data_by_token(token)), 200

@app.route("/get_user_data_by_email")
def get_user_data_by_email():
    database_helper.connect_db()
    email = request.args.get('email')
    token = request.args.get('token')
    return  jsonify(database_helper.get_user_data_by_email(token,email)), 200

@app.route("/get_user_messages_by_token")
def get_user_messages_by_token():
    database_helper.connect_db()
    token = request.args.get('token')
    return jsonify(database_helper.get_user_messages_by_token(token)), 200

@app.route("/get_user_messages_by_email")
def get_user_messages_by_email():
    database_helper.connect_db()
    token = request.args.get('token')
    email = request.args.get('email')
    return jsonify(database_helper.get_user_messages_by_email(token, email)), 200

@app.route("/post_message", methods=['POST','GET'])
def post_message():
    if request.method == 'POST':
        database_helper.connect_db()
        resJson = request.get_json()
        return  jsonify(database_helper.post_message(resJson["token"],resJson["message"],resJson["email"])), 200
    else:
        return "need POST Method", 404







if __name__ == '__main__':
    app.run()


server = pywsgi.WSGIServer( ('', 8080), app,  handler_class=WebSocketHandler)
server.serve_forever()
