from flask import Flask
import json
from random import *
app = Flask(__name__)

@app.route("/sign_in")
def sign_in(email, password):
    if (users[email] != null && users[email].password == password):
        token = random.Random
        return("success": true, "message": "Successfully signed in.", "data": token)
    else:
        return("success": false, "message": "Wrong username or password.")

@app.route("/sign_up")
def sign_up(email,password,firstname,familyname,gender,city,country):
    if all(x in List for x in (email,password,firstname,familyname,gender,city,country)) == undefined:
        if any(y in List for y in (email,password,firstname,familyname,gender,city,country)) == "null":
            return("success": false, "message": "Form data missing or incorrect type.")
        else:
            return("success": true, "message": "Successfully created a new user.")
    else:
        return()"success": false, "message": "User already exists.")

@app.route("/sign_out")
def sign_out(email, password):
    if (loggedInUsers[token] != null){
        delete loggedInUsers[token];
        return("success": true, "message": "Successfully signed out.")
    else:
        return("success": false, "message": "You are not signed in.")
