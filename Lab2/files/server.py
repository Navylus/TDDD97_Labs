from flask import Flask
from flask import g
from flask import jsonify
from uuid import uuid4
from flask import request
from flask import Response
from gevent import pywsgi
import json
import sqlite3

#RETURN JSON
# return Response(resJson, mimetype='application/json')

app = Flask(__name__, static_url_path='/static')
DATABASE = 'twidder.db'

@app.route("/")
def home():
    return current_app.send_static_file("client.html")

@app.route("/sign_up")
def sign_up():
    connect_db()
    return sign_up("email", "password", "firstname", "familyname", "gender", "city" , "country")


@app.route("/sign_in", methods=['POST','GET'])
def sign_in():
    if request.method == 'POST':
        connect_db()
        resJson = request.get_json()
        return sign_in(resJson["email"],resJson["password"])
    else:
        return "need POST Method"


@app.route("/addTest")
def addContact():
    connect_db()
    insertTestUser()
    return "added"

@app.route("/sign_out", methods=['POST','GET'])
def sign_out():
    if request.method == 'POST':
        connect_db()
        resJson = request.get_json()
        return sign_out(resJson["token"])
    else:
        return "need POST Method"


@app.route("/change_password", methods=['POST','GET'])
def change_password():
    if request.method == 'POST':
        connect_db()
        resJson = request.get_json()
        return change_password(resJson["token"],resJson["oldPassword"],resJson["newPassword"])
    else:
        return "need POST Method"

@app.route("/get_user_data_by_token")
def get_user_data_by_token():
    connect_db()
    token = request.args.get('token')
    return  get_user_data_by_token(token)

@app.route("/get_user_data_by_email")
def get_user_data_by_email():
    connect_db()
    email = request.args.get('email')
    token = request.args.get('token')
    return  get_user_data_by_email(token,email)

@app.route("/get_user_messages_by_token")
def get_user_messages_by_token():
    connect_db()
    token = request.args.get('token')
    return get_user_messages_by_token(token)

@app.route("/get_user_messages_by_email")
def get_user_messages_by_email():
    connect_db()
    token = request.args.get('token')
    email = request.args.get('email')
    return get_user_messages_by_email(token, email)

@app.route("/post_message", methods=['POST','GET'])
def post_message():
    if request.method == 'POST':
        connect_db()
        resJson = request.get_json()
        return sign_in(resJson["token"],resJson["message"],resJson["email"])
    else:
        return "need POST Method"


#DataBase Functions


def connect_db():
    g.db = sqlite3.connect(DATABASE)


def insertTestUser():
    g.db.execute("delete from user where email = 'a'")
    g.db.execute("insert into user values (?,?,?,?,?,?,?,?)", ("a","a","a","a","a","a","a","a"))
    g.db.commit()

def close_db():
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()


def sign_in(email,pwd):
    result = []
    cursor = g.db.execute("select * from user where email = ? and password = ?", [email, pwd])
    rows = cursor.fetchall()
    cursor.close()
    token = str(uuid4())
    if len(rows) == 0:
        result.append({'success': False, 'Code': 404, "data": "None"})
    else:
        result.append({'success': True, 'Code': 200, "data": token})
        g.db.execute("update user set token=? WHERE email = ? AND password = ?", [token, email, pwd])
    return jsonify(result)


#email, password, firstname, familyname, gender, city and country
def sign_up(email, password, firstname, familyname, gender, city, country):
    result = []
    #check parameters :
    if(len(password) < 4):
        result.append({'success': False, 'Code': 404, "data": "Password too short"})
    elif email == "" or password =="" or firstname =="" or familyname =="" or gender =="" or city =="" or country =="":
        result.append({'success': False, 'Code': 404, "data": "A parameter is empty"})
    else:
        try:
            g.db.execute("insert into user values (?,?,?,?,?,?,?,?)", (email, password,"",firstname, familyname, gender, city, country))
            g.db.commit()
            result.append({'success': True, 'Code': 200, "data": "Sign up"})
        except:
            result.append({'success': False, 'Code': 404, "data": "User already in the base"})
    return jsonify(result)

def sign_out(token):
    result = []
    cursor = g.db.execute("select count(email) from user where token = ?",(token,))
    count = cursor.fetchall()
    if len(count) == 1:
        g.db.execute("update user set token='' where token = ?", (token,))
        g.db.commit()
        result.append({'success': True, 'Code': 200, "data": "Sign out"})
    else:
        result.append({'success': False, 'Code': 404, "data": "Can not sign-out the user"})
    return jsonify(result)


def change_password(token, oldPassword, newPassword):
    result = []
    cursor = g.db.execute("select * from user where token = ? and password = ?", [token, oldPassword])
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 0:
        result.append({'success': False, 'Code': 404, "data": "This user is not in the DataBase"})
    else:
        g.db.execute("update user set password=? WHERE token = ? AND password = ?", [newPassword, token, oldPassword])
        g.db.commit()
        result.append({'success': True, 'Code': 200, "data": "Password changed"})
    return jsonify(result)

def get_user_data_by_token(token):
    result = []
    cursor = g.db.execute("select * from user where token = ?", (token,))
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 0:
        result.append({'success': False, 'Code': 404, "data": "This user is not in the DataBase"})
    else:
        data = []
        #email, firstname, familyname, gender, city , country.
        data.append({'email': rows[0][0], 'firstname': rows[0][7], 'familyname': rows[0][6], 'gender': rows[0][3], 'city': rows[0][4], 'country': rows[0][5]})
        result.append({'success': True, 'Code': 200, "data": data})
    return jsonify(result)

def get_user_data_by_email(token, email):
    result = []
    cursor = g.db.execute("select * from user where token = ?", (token,))
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 0:
        result.append({'success': False, 'Code': 404, "data": "This user is not in the DataBase"})
    else:
        data = []
        cursor = g.db.execute("select * from user where email = ?", (email,))
        infos = cursor.fetchall()
        data.append({'email': infos[0][0], 'firstname': infos[0][7], 'familyname': infos[0][6], 'gender': infos[0][3], 'city': infos[0][4], 'country': infos[0][5]})
        result.append({'success': True, 'Code': 200, "data": data})
    return jsonify(result)

def get_user_messages_by_token(token):
    result = []
    cursor = g.db.execute("select email from user where token = ?",(token,))
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 1:
        cursor = g.db.execute("select content from messages where email=?",(rows[0][0]))
        messages = cursor.fetchall()
        cursor.close()
        all_messages = []
        for index in range(len(messages)):
            all_messages.append({'email':messages[index][0], 'content':messages[index][1]})
        result.append({'success': True, 'Code': 200, "data": all_messages})
    else:
        result.append({'success': False, 'Code': 404, "data": "Can not retrieve the messages"})
    return jsonify(result)

def get_user_messages_by_email(token, email):
    result = []
    cursor = g.db.execute("select email from user where token = ?",(token))
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 1:
        cursor = g.db.execute("select content from messages where email = ?",(email,))
        messages = cursor.fetchall()
        cursor.close()
        all_messages = []
        for index in range(len(messages)):
            all_messages.append({'email':messages[index][0], 'content':messages[index][1]})
        result.append({'success': True, 'Code': 200, "data": all_messages})
    else:
        result.append({'success': False, 'Code': 404, "data": "Can not retrieve the messages"})
    return jsonify(result)

def post_message(token, message, email):
    result = []
    cursor = g.db.execute("select email from user where token = (?)",(token,))
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 1:
        g.db.execute("insert into messages values (?,?)", (email, message))
        g.db.commit()
        result.append({'success': True, 'Code': 200, "data": "Message posted"})
    else:
        result.append({'success': False, 'Code': 404, "data": "Can not post the messages"})
    return jsonify(result)

if __name__ == '__main__':
    app.run()


server = pywsgi.WSGIServer( ('', 8080), app)
server.serve_forever()
