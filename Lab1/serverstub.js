/**
 * Serverstub.js
 * MODIFIED TO USE server.py
 **/
var serverstub = (function() {


  var serverstub = {
      signIn: function (email, password) {
        var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/sign_in";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                res = JSON.parse(req.responseText);
                if(res.success === true){
                    localStorage.setItem('auth', res.data);
                    displayView();
                }else{
                    document.getElementById('feedBack').innerText = res.message;
                }
            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify({email:email, password:password}));
      },

      postMessage: function (token, message, email) {
        var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/post_message";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                res = JSON.parse(req.responseText);
            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify({token:token,message:message,email:email}));
        return res;
      },

      getUserDataByToken: function (token) {
          var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/get_user_data_by_token";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                res = JSON.parse(req.responseText);
            }
        };
        req.open("POST", url+"/?token="+token, true);
        req.send();
        return res;
      },

      getUserDataByEmail: function (token, email) {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/get_user_data_by_email";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                res = JSON.parse(req.responseText);
            }
        };
        req.open("POST", url+"/?token="+token+"/?email="+email, true);
        req.send();
        return res;
      },

      getUserMessagesByToken: function (token) {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/get_user_messages_by_token";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                res = JSON.parse(req.responseText);
            }
        };
        req.open("POST", url+"/?token="+token, true);
        req.send();
        return res;
      },

      getUserMessagesByEmail: function (token, email) {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/get_user_messages_by_email";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                res = JSON.parse(req.responseText);
            }
        };
        req.open("POST", url+"/?token="+token+"/?email="+email, true);
        req.send();
        return res;
      },

      signOut: function (token) {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/sign_out";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                res = JSON.parse(req.responseText);
            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify({token:token}));
        return res;
      },

      signUp: function (inputObject) { // {email, password, firstname, familyname, gender, city, country}
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/sign_in";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                res = JSON.parse(req.responseText);
            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify(inputObject));
        return res;
      },

      changePassword: function (token, oldPassword, newPassword) {
                  var req = new XMLHttpRequest();
        var current = document.location.href;
        var url = current+"/change_password";

        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                res = JSON.parse(req.responseText);
            }
        };
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type","application/json");
        req.send(JSON.stringify({token:token, oldPassword:oldPassword, newPassword:newPassword}));
        return res;
      }
  };

  return serverstub;
})();