// Show login page or home tab
displayView = function(){
    var auth = localStorage.getItem('auth');
    if (auth === "null" || auth === null ) {

        var text = document.getElementById('welcomeView').innerHTML;
        document.getElementById("content").innerHTML= text;
        attachHandlersLogin();

    }
    else {
        var text = document.getElementById('profileView').innerHTML;
        document.getElementById("content").innerHTML= text;
        attachHandlersProfile();
        setProfil();
        showWall();
        document.getElementById("Home").style.display = "block";
        var colorBG = localStorage.getItem('colorBG');
        if(colorBG !== "null" || colorBG !== null ){
            document.body.style.backgroundColor = colorBG;
        }
        $(function(){
            $('.backgroundPicker').colorpicker();
        });
    }
};


attachHandlersLogin = function(){

    var signin = document.getElementById("signin");
    var login = document.getElementById("login");

    document.getElementById('login').addEventListener('click', function() {
        login.setAttribute("onsubmit", "return false;");
        loginUser(this);
    });

    document.getElementById('signin').addEventListener('click', function() {
        signin.setAttribute("onsubmit", "return false;");
        signup(this);
    });


    var password = document.getElementById('repeatpassword');
    password.addEventListener("keyup", function () {
        var pswd = document.getElementById('passwordSignIn').value;
        var pswd2 = password.value;
        if(pswd2 !== pswd) {
            document.getElementById('feedBack').innerText = "Passwords do not match.";
            document.getElementById("signin").disabled = true;
        }else{
            document.getElementById('feedBack').innerText = "FeedBack Box";
            document.getElementById("signin").disabled = false;
        }
    });
};


attachHandlersProfile = function(event){

    var searchingEmail = "";
    document.getElementById('home').addEventListener('click', function() {
        openMenu(event, 'Home');
    });

    document.getElementById('browse').addEventListener('click', function() {
        openMenu(event, 'Browse')
    });

    document.getElementById('account').addEventListener('click', function() {
        openMenu(event, 'Account')
    });


    var password = document.getElementById('newConfirm');
    password.addEventListener("keyup", function () {
        var pswd = document.getElementById('new').value;
        var pswd2 = password.value;
        if(pswd2 !== pswd) {
            document.getElementById('feedbackProfile').innerText = "Passwords do not match.";
            document.getElementById("change").disabled = true;
        }else{
            document.getElementById('feedbackProfile').innerText = "FeedBack Box";
            document.getElementById("change").disabled = false;
        }
    });

    document.getElementById('change').addEventListener('click', function() {
        var token = localStorage.getItem('auth');
        var oldPwd = document.getElementById("old").value.trim();
        var newPwd = document.getElementById("new").value.trim();
        changePassword(token,oldPwd,newPwd);
    });


    document.getElementById('signoutBTN').addEventListener('click', function() {
        var token = localStorage.getItem("auth");
        serverstub.signOut(token);
    });

    document.getElementById('post').addEventListener('click', function() {
        serverstub.getUserDataByTokenAndPost();
    });


    document.getElementById('postOther').addEventListener('click', function() {
        var token = localStorage.getItem("auth");
        var message = document.getElementById("messageTextOther").value;
        if(message !== "")
            serverstub.postMessage(token,message,searchingEmail,true);
        showOtherWall(searchingEmail);
        document.getElementById("messageTextOther").value = "";
    });

    document.getElementById('reload').addEventListener('click', function() {
        showWall();
    });

    document.getElementById('searchBtn').addEventListener('click', function() {
        var searchEmail = document.getElementById('emailSearch').value;
        searchingEmail = searchEmail;
        showOtherWall(searchEmail);
    });

    document.getElementById('reloadOther').addEventListener('click', function() {
        showOtherWall(searchingEmail);
    });


};

// Sign up
function signup(){
    // {email, password, firstname, familyname, gender, city, country}
    var dataObject = {
        "email": document.getElementById('emailSignIn').value.trim(),
        "password": document.getElementById('passwordSignIn').value.trim(),
        "firstname": document.getElementById('firstname').value.trim(),
        "familyname": document.getElementById('lastname').value.trim(),
        "gender": document.getElementById('gender').value.trim(),
        "city": document.getElementById('city').value.trim(),
        "country": document.getElementById('country').value.trim()
    };
    serverstub.signUp(dataObject);
}

// Log in
function loginUser(){
    var email = document.getElementById('email').value.trim();
    var password = document.getElementById('password').value.trim();
    serverstub.signIn(email,password);
}



//TABS

function openMenu(evt, menu) {

    var i, tabcontent, tablinks;


    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }


    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }


    document.getElementById(menu).style.display = "block";
}

// Change password
function changePassword(token,oldpwd, newpwd){
    return serverstub.changePassword(token,oldpwd, newpwd)
}

//Show user infromation (home tab)
function setProfil(){ var req = new XMLHttpRequest();
        var current = document.location.href;
        var token = localStorage.getItem("auth");
        var url = current+"/get_user_data_by_token";
        var res = "";
        req.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                    //Show user infromation (home tab)
                var data = JSON.parse(req.responseText).data;
                var list = document.createElement('ul');


                var item = document.createElement('li');
                item.appendChild(document.createTextNode("Email : " + data.email));

                list.appendChild(item);


                item = document.createElement('li');
                item.appendChild(document.createTextNode("First name : " + data.firstname));

                list.appendChild(item);

                item = document.createElement('li');
                item.appendChild(document.createTextNode("Family name : " + data.familyname));

                list.appendChild(item);


                item = document.createElement('li');
                item.appendChild(document.createTextNode("Gender : " + data.gender));

                list.appendChild(item);


                item = document.createElement('li');
                item.appendChild(document.createTextNode("City : " + data.city));

                list.appendChild(item);


                item = document.createElement('li');
                item.appendChild(document.createTextNode("Country : " + data.country));

                list.appendChild(item);

                document.getElementById("infos").appendChild(list);
            }
        };
        req.open("GET", url+"?token="+token, true);
        req.send();
        return res;
}

// Wall of user
function showWall(){
    serverstub.getUserMessagesByToken();

}

// Wall of another searched user
function showOtherWall(email){
     var token = localStorage.getItem("auth");
    serverstub.getUserMessagesByEmail(token,email);
}

function allowDrop(ev) {
    ev.preventDefault();
}


function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    localStorage.setItem('colorBG', data);
    document.body.style.backgroundColor = data;
}


function drag(ev){
    ev.dataTransfer.setData("text", $(".backgroundPicker").colorpicker('getValue'));
}


function openConnection(user){
    ws = new WebSocket("ws://" + document.domain + ":8080/api");

    ws.onopen = function(){
        ws.send(user);
    };

    ws.onmessage = function(event){
        if(JSON.parse(event.data) === "LOGOUT"){
            ws.close();
            localStorage.setItem("auth",null);
            displayView();
        }
    };
}


window.onload = function(){
    displayView();
};
