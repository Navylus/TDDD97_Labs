#DataBase Functions
import json
import hashlib
import sqlite3
from flask import g
from uuid import uuid4
from flask import Flask
from flask import jsonify
from flask import request
from gevent import pywsgi
from flask import Response
from flask import current_app
from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler

DATABASE = 'twidder.db'


def connect_db():
    g.db = sqlite3.connect(DATABASE)


def insertTestUser():
    g.db.execute("delete from user where email = 'a'")
    g.db.execute("insert into user values (?,?,?,?,?,?,?,?)", ("a","a","a","a","a","a","a","a"))
    g.db.commit()

def close_db():
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()


def sign_in(email,password,token):
    pwd = hashlib.sha224(password.encode('utf-8')).hexdigest()
    cursor = g.db.execute("select * from user where email = ? and password = ?", [email, pwd])
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 0:
        result = ({'success': False, 'message': "user not found in the database", "data": "None"})
    else:
        g.db.execute("update user set token= ? WHERE email = ? AND password = ?", [token, email, pwd])
        g.db.commit()
        result = ({'success': True, 'message': "OK", "data": token})
    return result


#email, password, firstname, familyname, gender, city and country
def sign_up(email, password, firstname, familyname, gender, city, country):
    result = []
    #check parameters :
    if(len(password) < 4):
        result = ({'success': False, 'message': "Password too short", "data": "Password too short"})
    elif email == "" or password =="" or firstname =="" or familyname =="" or gender =="" or city =="" or country =="":
        result = ({'success': False, 'message': "A parameter is empty", "data": "A parameter is empty"})
    else:
        try:
            pwd = hashlib.sha224(password).hexdigest()
            g.db.execute("insert into user values (?,?,?,?,?,?,?,?)", (email, pwd,"",firstname, familyname, gender, city, country))
            g.db.commit()
            result = ({'success': True, 'message': "Sign up succes", "data": "Sign up succes"})
        except:
            result = ({'success': False, 'message': "User already in the base", "data": "User already in the base"})
    return result

def sign_out(token):
    cursor = g.db.execute("select count(email) from user where token = ?",(token,))
    count = cursor.fetchall()
    if len(count) == 1:
        g.db.execute("update user set token='' where token = ?", (token,))
        g.db.commit()
        result = ({'success': True, 'message': "Sign out", "data": "Sign out"})
    else:
        result = ({'success': False, 'message': "Can not sign-out the user", "data": "Can not sign-out the user"})
    return result


def change_password(token, oldPassword, newPassword):
    oldpwd = hashlib.sha224(oldPassword).hexdigest()
    newpwd = hashlib.sha224(newPassword).hexdigest()
    cursor = g.db.execute("select * from user where token = ? and password = ?", [token, oldpwd])
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 0:
        result = ({'success': False, 'message': "This user is not in the DataBase", "data": "This user is not in the DataBase"})
    else:

        g.db.execute("update user set password=? WHERE token = ? AND password = ?", [newpwd, token, oldpwd])
        g.db.commit()
        result = ({'success': True, 'message': "Password changed", "data": "Password changed"})
    return result

def get_user_data_by_token(token):
    cursor = g.db.execute("select * from user where token = ?", (token,))
    infos = cursor.fetchall()
    cursor.close()
    if len(infos) == 0:
        result = ({'success': False, 'message': "This user is not in the DataBase", "data": "This user is not in the DataBase"})
    else:

        #email, firstname, familyname, gender, city , country.
        data = ({'email': infos[0][0], 'firstname': infos[0][3], 'familyname': infos[0][4], 'gender': infos[0][5], 'city': infos[0][6], 'country': infos[0][7]})
        result = ({'success': True, 'message': "OK", "data": data})
    return result

def get_user_data_by_email(token, email):
    cursor = g.db.execute("select * from user where token = ?", (token,))
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 0:
        result = ({'success': False, 'message': "This user is not in the DataBase", "data": "This user is not in the DataBase"})
    else:
        cursor = g.db.execute("select * from user where email = ?", (email,))
        infos = cursor.fetchall()
        data = ({'email': infos[0][0], 'firstname': infos[0][3], 'familyname': infos[0][4], 'gender': infos[0][5], 'city': infos[0][6], 'country': infos[0][7]})


        result = ({'success': True, 'message': "OK", "data": data})
    return result

def get_user_messages_by_token(token):
    cursor = g.db.execute("select email from user where token = ?",(token,))
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 1:
        cursor = g.db.execute("select * from messages where email=?",(rows[0][0],))
        messages = cursor.fetchall()
        cursor.close()
        all_messages = []

        for index in range(len(messages)):
            all_messages.append({'email':messages[index][1], 'content':messages[index][2]})
        result = ({'success': True, 'message': "OK", "data": all_messages})
    else:
        result = ({'success': False, 'message': "Can not retrieve the messages", "data": "Can not retrieve the messages"})
    return result

def get_user_messages_by_email(token, email):
    cursor = g.db.execute("select email from user where token = ?",(token,))
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 1:
        cursor = g.db.execute("select * from messages where email = ?",(email,))
        messages = cursor.fetchall()
        cursor.close()
        all_messages = []
        for index in range(len(messages)):
            all_messages.append({'email':messages[index][1], 'content':messages[index][2]})
        result = ({'success': True, 'message': "OK", "data": all_messages})
    else:
        result = ({'success': False, 'message': "Can not retrieve the messages", "data": "Can not retrieve the messages"})
    return result

def post_message(token, message, email):
    cursor = g.db.execute("select email from user where token = ?",(token,))
    rows = cursor.fetchall()
    cursor.close()
    if len(rows) == 1:
        g.db.execute("insert into messages values (?,?,?)", (email,rows[0][0], message))
        g.db.commit()
        result = ({'success': True, 'Message': "OK", "data": "Message posted"})
    else:
        result = ({'success': False, 'Message': "Can not post the messages", "data": "Can not post the messages"})
    return result


